﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizzFuzz.Test
{
    [TestFixture]
    public class BizzFuzzTest
    {
        [Test]
        public void GetList_CompareStrings_1()
        {
            BizzFuzz bizz = new BizzFuzz();
            List<string> results = bizz.GetList(100);
            Assert.AreEqual("1", results.ToArray()[0]);
        }

        [Test]
        public void GetList_CompareStrings_2()
        {
            BizzFuzz bizz = new BizzFuzz();
            List<string> results = bizz.GetList(100);

            Assert.AreEqual("Bizz", results.ToArray()[8]);
        }

        [Test]
        public void GetList_CompareStrings_3()
        {
            BizzFuzz bizz = new BizzFuzz();
            List<string> results = bizz.GetList(100);

            Assert.AreEqual("Bizz Fuzz", results.ToArray()[14]);
        }
    }
}
