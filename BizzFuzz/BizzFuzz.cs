﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizzFuzz
{
    public class BizzFuzz
    {
        public List<string> GetList(short count)
        {
            List<string> result = new List<string>();
            string element;

            for (short i = 1; i <= count; i++)
            {
                element = string.Empty;
                if (i % 3 == 0)
                    element = "Bizz";

                if (i % 5 == 0)
                    element += " Fuzz";

                if ((i % 5) != 0 && (i % 3) != 0)
                    element = i.ToString();

                result.Add(element.Trim());
            }
            return result;
        }
    }
}
